# Digital Fabrication 2022

## DF III

1. Darren Bratten  
  https://gitlab.com/darrenbratten/digital-fabrication_2022/  
  https://darrenbratten.gitlab.io/digital-fabrication_2022/

1. Fiia Kitinoja  
  https://gitlab.com/fiia.kitinoja/digital-fabrication/  
  https://digital-fabrication.fiikuna.fi/

1. Anna Li  
  https://gitlab.com/yeshouanna/aalto-digital-fabrication-2022  
  https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/

1. Karl Mihhels  
  https://gitlab.com/karlpalo/fablab2022/  
  https://karlpalo.gitlab.io/fablab2022/

1. Arthur Tollet    
  https://gitlab.fabcloud.org/academany/fabacademy/2022/labs/aalto/students/arthur-tollet   
  https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/

1. Yu-Han Tseng  
  https://gitlab.com/YuhanTYH/digital-fabrication/  
  https://YuhanTYH.gitlab.io/digital-fabrication/

1. Yikun Wang  
  https://gitlab.com/yikun_wang/digital-fabrication/  
  https://yikun_wang.gitlab.io/digital-fabrication/

## DF II

1. Darren Bratten  
  https://gitlab.com/darrenbratten/digital-fabrication_2022/  
  https://darrenbratten.gitlab.io/digital-fabrication_2022/

1. Fiia Kitinoja  
  https://gitlab.com/fiia.kitinoja/digital-fabrication/  
  https://digital-fabrication.fiikuna.fi/

1. Samuli Kärki  
  https://gitlab.com/save.points/digital-fabrication/  
  https://ambivalent.world/

1. Anna Li  
  https://gitlab.com/yeshouanna/aalto-digital-fabrication-2022  
  https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/

1. Karl Mihhels  
  https://gitlab.com/karlpalo/fablab2022/  
  https://karlpalo.gitlab.io/fablab2022/

1. Arthur Tollet    
  https://gitlab.fabcloud.org/academany/fabacademy/2022/labs/aalto/students/arthur-tollet   
  https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/

1. Yu-Han Tseng  
  https://gitlab.com/YuhanTYH/digital-fabrication/  
  https://YuhanTYH.gitlab.io/digital-fabrication/

1. Yikun Wang  
  https://gitlab.com/yikun_wang/digital-fabrication/  
  https://yikun_wang.gitlab.io/digital-fabrication/

1. Yoona Yang  
  https://gitlab.com/yyoona/digital-fabrication/  
  https://yyoona.gitlab.io/digital-fabrication/

## DF I

1. Kathryn Ballinger  
  https://gitlab.com/kitbee/digital-fabrication/  
  https://kitbee.gitlab.io/digital-fabrication/

1. Darren Bratten  
  https://gitlab.com/darrenbratten/digital-fabrication_2022/  
  https://darrenbratten.gitlab.io/digital-fabrication_2022/

1. Ida Budolfsen  
  https://gitlab.com/Ibununu/digital-fabrication/  
  https://ibununu.gitlab.io/digital-fabrication/  

1. Ya-Ning Chang  
  https://gitlab.com/ningchang/digital-fabrication/  
  https://ningchang.gitlab.io/digital-fabrication/

1. Calvin Guillot  
  https://gitlab.com/calvinguillot/digital-fabrication/  
  https://calvinguillot.gitlab.io/digital-fabrication/

1. Monika Hauck  
  https://gitlab.com/monhauck/digital-fabrication/  
  https://monhauck.gitlab.io/digital-fabrication/

1. Inka Jerkku  
  https://gitlab.com/InkaJ/digital-fabrication  
  https://inkaj.gitlab.io/digital-fabrication/

1. Fiia Kitinoja  
  https://gitlab.com/fiia.kitinoja/digital-fabrication/  
  https://digital-fabrication.fiikuna.fi/

1. Samuli Kärki  
  https://gitlab.com/save.points/digital-fabrication/  
  https://ambivalent.world/

1. Anna Li  
  https://gitlab.com/yeshouanna/aalto-digital-fabrication-2022  
  https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/

1. Carlos Perez Moreno  
  https://gitlab.com/presudo/digital-fabrication/  
  https://presudo.gitlab.io/digital-fabrication/

1. Jason Selvarajan  
  https://gitlab.com/jasoncake/s21  
  https://jasoncake.gitlab.io/s21

1. Yu-Han Tseng  
  https://gitlab.com/YuhanTYH/digital-fabrication/  
  https://YuhanTYH.gitlab.io/digital-fabrication/

1. Yikun Wang  
  https://gitlab.com/yikun_wang/digital-fabrication/  
  https://yikun_wang.gitlab.io/digital-fabrication/

1. Yoona Yang  
  https://gitlab.com/yyoona/digital-fabrication/  
  https://yyoona.gitlab.io/digital-fabrication/

1. Yujie Zhou  
  https://gitlab.com/yujiezhou/digital-fabrication/   
  https://yujiezhou.gitlab.io/digital-fabrication/

1. Arthur Tollet    
  https://gitlab.fabcloud.org/academany/fabacademy/2022/labs/aalto/students/arthur-tollet   
  https://fabacademy.org/2022/labs/aalto/students/arthur-tollet/
